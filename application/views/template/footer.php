<footer id="rodape-global">
	<div class="container text-center">
		<p> 
			<span class="small color-gold"> G3 -> </span>
			<a href="https://www.linkedin.com/in/leandro-falcao/" target="_blank" rel="noopener noreferrer">
				<span class="small" >leandro falcao</span>
			</a> 
			<a href="http://moozthemes.com">
					<span class="small2"> tema base - by moozthemes</span> 
			</a>
		</p>
	</div>
</footer>
<script>
	const elementFooter = document.querySelector("#rodape-global")
	console.log('elemento do footer: ', elementFooter);

	const classSmall = `.small{
		font-size:1.19rem;
	}`;
	const maisSmall = `.small2{
		font-size:0.69rem;
		color: #eef;
	}`;
	
	const colorGold = `.color-gold{
		color: #ffd700;
	}`;

	const style = `<style> \n ${classSmall} \n ${maisSmall} \n ${colorGold} \n </style>`;

	elementFooter.insertAdjacentHTML("afterend", style)
	console.log('elemento do footer: ', elementFooter);

</script>


<!-- <section style="min-height: calc(100vh - 83px)" class="light-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6 text-center">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>LOGIN</h2>
						</div>
						<form id="login_form" method="post">
							
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-user"></span>
											</div>
											<input type="text" placeholder="Usuário" id="username" name="username"
											class="form-control">
										</div>
										<span class="help-block"></span>
									</div>
							

							
									<div class="form-group">
										<div class="input-group">

											<div class="input-group-addon">
												<span class="glyphicon glyphicon-lock"></span>
											</div>

											<input type="password" placeholder="Senha" name="password"
											class="form-control">
										</div>
									</div>
							
									<div class="form-group">
										<button type="submit" id="btn_login" class="btn btn-block">Login</button>
									</div>
									<span class="help-block"></span>
							

						</form> 
					</div>
				</div>
			</div>
		</div>

	</div>
	
</section> -->

<section id="altura-login" class="light-bg style-login"
      style="">
      <div class="container">

         <div class="row logi-senha-user">
            <!-- input do usuario -->
            <div class="col-lg-offset-3 col-lg-6 text-center">
               <div class="section-title">
                  <h2>LOGIN</h2>

                  <form id="login_form" action="" method="post"> <!-- formulario para enviar para logar o usuário -->

                     <div class="row row-user-data">
                        <div class="col-lg-12">
                           <div class="form-group">
                              <div class="input-group">
                                 <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                 </div>
                                 <input type="text" name="username" class="form-control user" placeholder="Usuário">
                              </div>
                           </div>
                        </div>
                     </div> <!--end row de nome de usuario do form -->

                     <div class="row row-password-data">
                        <!-- input da senha -->
                        <div class="col-lg-12">
                           <div class="form-group">
                              <div class="input-group">
                                 <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                 </div>
                                 <input type="password" name="password" class="form-control password" placeholder="Senha">
                              </div>
                           </div>
                        </div>
                     </div> <!--end row de password do form -->


                     <div class="row row-input-send">
                        <!-- input/buton para enviar o form  -->
                        <div class="col-lg-12">
                           <div class="form-group">
                              <button type="submit" id="btn-login" class="btn btn-block">LOGIN</button>
                           </div>
                           <span class="help-block"></span>
                        </div>

                     </div> <!--end row de input submit form-->

                  </form> <!--fim fo form de usuario para login -->
               </div>
            </div>
         </div> <!-- end div row  principal-->

      </div>
      <!-- end do container -->
   </section>

   <!-- colocando o css dentro pagina especifica -->
   <script>
      const stylePageLogin = document.querySelector(".style-login")
      console.log('antes de setar os dados: ', stylePageLogin);
      stylePageLogin.setAttribute('style', `min-height: calc(100vh - 80px);
         background-color: #0000ff;
         background-image: linear-gradient(158deg, #286090 25%, #a3b6cd 49%, #1fa 67%, #cec4cc 80%, #eC8 91%, #cfa 94%);`)


   </script>